### Tech task
[![Build Status](https://gitlab.com/henry.band/task/badges/master/pipeline.svg)](https://gitlab.com/henry.band/task/-/commits/master)

GitHub web crawler based on the GitHub search

```python
from crawler import GithubCrawler

bot = GithubCrawler(
    keywords=keywords,
    proxies=proxies, 
    search_type=search_type,
)
results = bot.crawl()

```
#### Arguments
* **keywords** List of keywords to be used as search terms
* **object_type** The search type of object:
    * Repositories
    * Issues
    * Wikis
* **proxies** - List of proxies

#### Instalation
Install with pip into python (virtual environment) extra `requrements.txt`:
```bash
$ virtualenv -p python3 venv
...
$ source venv/bin/activate
$ pip install -r requirements.txt
```

#### Examples
##### Repositories
Python code
```python
bot = GithubCrawler(
    keywords=['openstack', 'nova', 'css'],
    proxies=[
        "149.125.151.80:8080",
        "51.222.29.216:8080",
    ],
    search_type='Repositories'
)
bot.crawl()
```
Output
```python
[
  {
    "url": "https://github.com/atuldjadhav/DropBox-Cloud-Storage",
    "extra": {
      "owner": "atuldjadhav",
      "language_stats": {
        "CSS": 52.0,
        "JavaScript": 47.2,
        "HTML": 0.8
      }
    }
  },
  {
    "url": "https://github.com/michealbalogun/Horizon-dashboard",
    "extra": {
      "owner": "michealbalogun",
      "language_stats": {
        "Python": 100.0
      }
    }
  }
]
```
##### Issues
```python
bot = GithubCrawler(
    keywords=['vcrpy', 'threading', 'session'],
    proxies=[
        "149.125.151.80:8080",
        "51.222.29.216:8080",
    ],
    search_type='Issues'
)
bot.crawl()
```
Output
```bash
[
  {"url": "https://github.com/napalm255/ansible-vmware_inventory/pull/49"},
  {"url": "https://github.com/madjar/nox/issues/73"},
  {"url": "https://github.com/napalm255/ansible-vmware_inventory/pull/48"},
  {"url": "https://github.com/napalm255/ansible-vmware_inventory/pull/47"},
  {"url": "https://github.com/obspy/obspy/pull/1663"},
  {"url": "https://github.com/freedomofpress/securedrop-client/pull/788"},
  {"url": "https://github.com/kevin1024/vcrpy/issues/388"},
  {"url": "https://github.com/gentoo/gentoo/pull/18887"},
  {"url": "https://github.com/NixOS/nixpkgs/pull/88613"},
  {"url": "https://github.com/gentoo/gentoo/pull/15135"}
]
```

### Tests / Coverage
```bash
$ pytest --cov=crawler --cov-report term-missing
============================= test session starts ==============================
platform linux -- Python 3.9.4, pytest-6.2.3, py-1.10.0, pluggy-0.13.1
rootdir: /builds/henry.band/task
plugins: requests-mock-1.8.0, cov-2.11.1
collected 6 items
tests/test_crawler.py ......                                             [100%]
----------- coverage: platform linux, python 3.9.4-final-0 -----------
Name         Stmts   Miss  Cover   Missing
------------------------------------------
crawler.py      78      1    99%   99
------------------------------------------
TOTAL           78      1    99%
============================== 6 passed in 0.37s ===============================
Cleaning up file based variables
00:01
Job succeeded
```