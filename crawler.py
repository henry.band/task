from random import choice
import logging
import re
import requests

from urllib import parse as urlib_parse

NETLOC = 'https://github.com/'
SEARCH_PATH = '/search'

# ToDo: extract the lines to the config and store as yaml (config client usage)
SEARCH_MAX_PAGES = 1
MAX_ATTEMPTS = 3
HEADERS = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
              'application/signed-exchange;v=b3;q=0.9',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-US,en;q=0.9',
    # ToDo: Add random choice selection from the list of most common user agents
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/89.0.4389.128 Safari/537.36',
}

# The same logic can be done using lxml, but since the efficiency is critical and a extremely big scale
# it might matter I used regexps for this task since they are technically better in terms of performance
# since lxml parses and builds a the whole structure (tree).
# In practice I used both lxml/xpath approaches, regex approaches and JSON extraction for cases of
# JSON being rendered inside the response html (as script part).
REGEX_TYPE_MAPPING = {
    'Repositories': re.compile(r'repo-list-item[\s\S]*?<a class="v-align-middle".*?href="(.*?)"'),
    'Issues': re.compile(r'issue-list-item[\s\S]*?data-hydro-click=.*?href="(.*?)"'),
    'Wikis': re.compile(r'hx_hit-wiki[\s\S]*?href="([^"]*?wiki[^"]*?)"'),
}

RE_LANGUAGE_STATS_ELEMENT = re.compile(r'class="Progress ">([\s\S]*?)</span></div>')
RE_LANGUAGE_STATS = re.compile(r'span.*?aria-label="(.*?) ([\.\d]*)"')


class CrawlerError(BaseException):
    """Custom exception for handling scraping related errors."""


class SessionMixin:
    """Session mixin for extraction and splitting responsibilities between classes."""
    proxies = []

    def configure_session(self):
        """Method for configuring everything related to session, for example custom proxy provider
        or retry mechanism logic bound to whole session / cookies and etc.
        """
        self.session = requests.Session()
        self.session.headers.update(HEADERS)

    def _request(self, url, proxy, params=None):
        response = self.session.get(
            url=url,
            params=params,
            proxies={'https': proxy},
            stream=True,
        )
        response.raise_for_status()
        return response

    def request_with_retries(self, url, params=None):
        """Perform requests with retries and switching proxies between.

        :param url: destination url
        :param params: query string parameters
        :return: response object
        """
        request_attempt = 0

        while request_attempt < MAX_ATTEMPTS and self.proxies:
            proxy = self.get_proxy()

            try:
                response = self._request(
                    url=url,
                    params=params,
                    proxy=proxy,
                )
                return response
            except Exception as exc:  # ToDo replace with the list of exceptions handled
                self.session.cookies.clear()
                request_attempt += 1
                logging.info('%s happened during opening %s - switching to another proxy' % (exc, url))
                self.proxies.remove(proxy)

        raise CrawlerError('Max retries reached')

    def get_proxy(self):
        """
        :raise: Exception in case of all proxies being used
        :return: Random proxy
        """

        if self.proxies:
            return choice(self.proxies)
        raise CrawlerError('Proxies exhausted')


class GithubCrawler(SessionMixin):
    def __init__(self, keywords=(), proxies=(), search_type='Repositories'):
        self.proxies = proxies
        self.keywords = keywords
        self.search_type = search_type

        self.configure_session()
        self.page_number = 1

    def crawl(self):
        search_result = self.perform_search(self.keywords)
        links = self.extract_links(self.search_type, search_result)

        if self.search_type == 'Repositories':
            # the following code might be done as a list comprehension but it would make it rather complex to debug.
            result = []
            for link in links:
                result.append({'url': link, 'extra': self.extract_repo_data(link)})
        else:
            result = [{'url': link} for link in links]
        self.session.close()
        return result

    def perform_search(self, keywords):
        search_string = ' '.join(keywords)
        query_params = {'p': self.page_number, 'q': search_string, 'type': self.search_type}
        search_url = urlib_parse.urljoin(NETLOC, SEARCH_PATH)
        search_result = self.request_with_retries(
            search_url,
            params=query_params,
        )
        return search_result

    @staticmethod
    def extract_links(type, search_result):
        """Extract paths to the items and form final links."""
        return [urlib_parse.urljoin(NETLOC, path) for path in REGEX_TYPE_MAPPING[type].findall(search_result.text)]

    def extract_repo_data(self, link):
        owner = urlib_parse.urlparse(link).path.split('/')[1]
        try:
            repo_page = self.request_with_retries(link)
        except CrawlerError:
            logging.error('Failed to open %s' % link)
            return {
                'owner': owner,
                'language_stats': 'Error processing repo page',
            }

        repo_stats_string = RE_LANGUAGE_STATS_ELEMENT.search(repo_page.text).group(0)
        if repo_stats_string:
            repo_stats = RE_LANGUAGE_STATS.findall(repo_stats_string)
            extra_stats = dict((name, float(percentage)) for name, percentage in repo_stats)
            return {
                'owner': owner,
                'language_stats': extra_stats,
            }
