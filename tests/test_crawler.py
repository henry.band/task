import crawler
import json
import os
import responses
import unittest

DIR = os.path.dirname(__file__)

TEST_PROXIES = [
    "149.125.151.80:8080",
    "51.222.29.216:8080",
    "51.81.82.175:80",
    "54.165.67.102:8080",
    "85.31.246.47:3218",
]  # Proxies might get outdated since its free proxies from https://free-proxy-list.net/

RESPONSE_MOCK_MAPPING = {
    'Issues': [('https://github.com/search?p=1&q=vcrpy+threading+session&type=Issues', 'issue_search_result.html')],
    'Wikis': [('https://github.com/search?p=1&q=vcrpy&type=Wikis', 'wiki_search_result.html')],
    'Repositories': [
        ('https://github.com/search?p=1&q=openstack+nova+css&type=Repositories', 'repo_search_result.html'),
        ('https://github.com/atuldjadhav/DropBox-Cloud-Storage', 'repo_result_item_01.html'),
        ('https://github.com/michealbalogun/Horizon-dashboard', 'repo_result_item_02.html'),
    ],
}


class TestBase(unittest.TestCase):
    expected_data_file = None
    keywords = ()
    search_type = 'Repositories'

    def setUp(self):
        super().setUp()
        self.proxies = TEST_PROXIES.copy()

    @staticmethod
    def open_file(path):
        with open(os.path.join(DIR, path)) as f:
            return f.read()

    def mock_page_responses(self):
        for r in RESPONSE_MOCK_MAPPING[self.search_type]:
            responses.add(responses.GET, url=r[0], body=self.open_file(f'fixtures/{r[1]}'))

    @responses.activate
    def perform_crawl(self):
        self.mock_page_responses()
        bot = crawler.GithubCrawler(keywords=self.keywords, proxies=self.proxies, search_type=self.search_type)
        return bot.crawl()

    def check_results(self):
        results = self.perform_crawl()
        expected_results = json.loads(self.open_file(f'expected/{self.expected_data_file}'))
        self.assertEqual(expected_results, results)


class RepoTestCase(TestBase):
    keywords = ['openstack', 'nova', 'css']
    expected_data_file = 'repo_results.json'

    def test_bot(self):
        self.check_results()


class IssueTestCase(TestBase):
    keywords = ['vcrpy', 'threading', 'session']
    expected_data_file = 'issue_results.json'
    search_type = 'Issues'

    def test_bot(self):
        self.check_results()


class WikiTestCase(TestBase):
    keywords = ['vcrpy']
    expected_data_file = 'wiki_results.json'
    search_type = 'Wikis'

    def test_bot(self):
        self.check_results()


class ProxiesExhaustedTestCase(WikiTestCase):
    keywords = ['vcrpy']
    expected_data_file = 'wiki_results.json'
    search_type = 'Wikis'

    def setUp(self):
        super().setUp()
        self.proxies = []

    def test_bot(self):
        with self.assertRaises(crawler.CrawlerError) as exc:
            self.check_results()
            self.assertEqual(exc.exception.args[0], 'Proxies exhausted')


class NegativeResponseTestCase(IssueTestCase):
    """Test proper exception being raised upon hitting 403."""

    def mock_page_responses(self):
        responses.add(
            responses.GET,
            url='https://github.com/search?p=1&q=vcrpy+threading+session&type=Issues',
            status=403,
        )

    def test_bot(self):
        self.assertRaises(crawler.CrawlerError, self.check_results)


class RepoFailureTestCase(RepoTestCase):
    """Test if one of the links is not being properly handled the rest are still extracted."""
    keywords = ['openstack', 'nova', 'css']
    expected_data_file = 'repo_results_with_error.json'

    def mock_page_responses(self):
        responses.add(
            responses.GET,
            url='https://github.com/search?p=1&q=openstack+nova+css&type=Repositories',
            body=self.open_file(f'fixtures/repo_search_result.html')
        )
        responses.add(
            responses.GET,
            url='https://github.com/atuldjadhav/DropBox-Cloud-Storage',
            body=self.open_file('fixtures/repo_result_item_01.html')
        )
        responses.add(
            responses.GET,
            url='https://github.com/michealbalogun/Horizon-dashboard',
            status=403,
        )

    def test_bot(self):
        self.check_results()
